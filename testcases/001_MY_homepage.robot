*** Setting ***
Resource    ../module/homepage.robot
Resource	../settings/settings.robot

#Setup/Tear Down
Test Setup	Test Case Setup	${platform}	${env}
# Test Teardown	Close Browser

# robot --nostatusrc -d Reports --timestampoutputs --log mylog.html --report report.html -i tc01 -V vars/iot.yaml -v remote_url:None  .\TestCases\001_tc_homepage.robot
*** Variables ***
${home_header_sub_links_container}    //ul[@class="header__menu"]
${home_footer_app_container}    //footer//div[@class="footer__engage"]
@{window}
${window1}
${home_footer_links_container}    //div[contains(@class, 'grid') and contains(@class ,'push--bottom')]

*** Keywords ***
launch carlist home page
	Go To	${url}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_carlist_logo}	timeout=12
	Set test variable	${title}
	${title}	Get Title

input username
	[Arguments]	${username}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${user_name_login}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Input text	${user_name_login}	${username}

input password
	[Arguments]	${password}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${password_login}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Input text	${password_login}	${password}

click sign in link
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_signin_link}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Click Element	${home_signin_link}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_signin_modal_container}	timeout=12

click sign in button
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_signin_button}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Click Element   ${home_signin_button}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${loggedin_user_avatar}	timeout=12

click on footer app
    [Arguments]     ${app}
    Set test variable   ${applink}    //footer//div[@class="footer__engage"]//a[contains(@href, "${app}")]
    Run Keyword and continue on Failure    Wait Until Element Is Visible   ${home_footer_app_container}
    Run Keyword and continue on Failure    Mouse Over   ${home_footer_app_container}
    Run Keyword and continue on Failure    Wait Until Element Is Visible    ${applink}
    Run Keyword and continue on Failure    Wait Until Keyword Succeeds   5x   500ms   click element   ${applink}

verify app links and title
    [Arguments]     ${apps}
    :For    ${index}    IN RANGE    3
    \   Sleep   0.5
    \   ${hrefc}=   Get Locations
    \   ${urlcount}=   Get length    ${hrefc}
    \   ${wintitle}=   Get Window Titles
    \   ${titlecount}=   Get length    ${wintitle}
    \   Run Keyword If   ${titlecount}==1    Continue For Loop
    \   ${2ndwindow_title}=    Get From List   ${wintitle}    -1
    \   ${2ndwindow_url}=    Get From List   ${hrefc}    -1
    \   Run Keyword and continue on Failure   Should Contain    ${2ndwindow_title}    ${apps}    ignore_case=True
    \   Run Keyword and continue on Failure   Should Contain    ${2ndwindow_url}    ${apps}    ignore_case=True
    \   Run Keyword If   ${titlecount}>1    Exit For Loop

wait until carlist homepage load
    :For    ${index}    IN RANGE    3
    \   Sleep   0.5
    \   ${wintitle}=   Get Window Titles
    \   Run Keyword If  '${wintitle}'=='Find new & used cars for sale in Malaysia - carlist.my'    Exit For Loop

verify header sub links
    [Arguments]     ${link}
    Set test variable   ${testlink}    //ul[@class="header__menu"]//span[text()="${link}"]
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_header_sub_links_container}
    Run Keyword and continue on Failure     Mouse Over   ${home_header_sub_links_container}
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${testlink}

verify header desktop menu links
    [Arguments]     ${link}
    Set test variable   ${testlink}    //ul[@class="header__menu"]//span[text()="${link}"]
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_header_sub_links_container}
    Run Keyword and continue on Failure     Mouse Over   ${home_header_sub_links_container}
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${testlink}

verify footer apps
    [Arguments]     ${app}
    Set test variable   ${applink}    //footer//div[@class="footer__engage"]/a[contains(@href, "${app}")]
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_footer_app_container}
    Run Keyword and continue on Failure     Mouse Over   ${home_footer_app_container}
    Run Keyword and continue on Failure     Wait Until Element Is Visible    ${applink}


verify footer links
    [Arguments]     ${link}
    Set test variable   ${applink}    //div[contains(@class, 'grid') and contains(@class ,'push--bottom')]//a[contains(@title,"${link}")]
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_footer_links_container}
    Run Keyword and continue on Failure     Mouse Over   ${home_footer_links_container}
    Run Keyword and continue on Failure     Wait Until Element Is Visible    ${applink}

click on footer links
    [Arguments]     ${link}
    Set test variable   ${applink}    //div[contains(@class, 'grid') and contains(@class ,'push--bottom')]//a[contains(@title,"${link}")]
    Run Keyword and continue on Failure    Wait Until Element Is Visible   ${home_footer_app_container}
    Run Keyword and continue on Failure    Mouse Over   ${home_footer_app_container}
    Run Keyword and continue on Failure    Wait Until Element Is Visible    ${applink}
    Run Keyword and continue on Failure    Wait Until Keyword Succeeds   5x   500ms   click element   ${applink}


*** Test Cases ***
TC01- launch carlist.my
	[Documentation]	This test is to launch carlist.my app
	[tags]	carlist	medium	tc01
	launch carlist home page
	click sign in link
	input username	${privateuser}
	input password	${privateuserpassword}
	click sign in button


TC02- verify header elements
    [Documentation]      Verify header element
    [tags]  carlist    medium    tc01
    launch carlist home page

    @{header_linksfull}    Create List    Buy   Sell   New Cars  Finance  News  Events  register   Sign In  Sell Your Car
    @{header_links}    Create List    Buy   Sell   New Cars  Finance  News  Events

    :FOR    ${link}  IN  @{header_links}
    \   verify header sub links    ${link}


TC03- Verify footer section links
    [Documentation]    Verify footer section links
    [tags]  carlist    medium    tc03
    launch carlist home page

    @{buy_and_sell_section_links}    Create List    Cars For Sale  New Car Deals  Sell Your Car  Trusted
    @{news_and_review_section_links}    Create List    Latest Car News  Car Reviews  Editorial Team
    @{product_and_service_links}    Create List    Dealership  CarlistBid.my  Lead Marketplace  iCarData  Event Services
    @{about_us_section_link}    Create List    About Carlist.my  Contact Us  FAQ  Sitemap
    @{our_network_section_link}    Create List    One2car.com  Mobil123.com  Livelifedrive.com  Autospinn.com  OtoSpirit.com  Carlist.my

    @{all_links}=  Combine lists  ${buy_and_sell_section_links}  ${product_and_service_links}  ${about_us_section_link}  ${our_network_section_link}


    :FOR    ${app}  IN  @{all_links}
    \   verify footer links     ${app}

    :FOR    ${app}  IN  @{all_links}
    \   click on footer links     ${app}
    \   verify app links and title     ${app}
    \   Go Back
    \   wait until carlist homepage load


TC04- Verify the footer app elements
    [Documentation]         Verify the footer - pages and apps link
    [tags]  carlist    medium    tc02
    launch carlist home page

    @{footer_app_links}    Create List    facebook   twitter   youtube     instagram
    :FOR    ${app}  IN  @{footer_app_links}
    \   verify footer apps     ${app}

    :FOR    ${app}  IN  @{footer_app_links}
    \   click on footer app     ${app}
    \   verify app links and title     ${app}




