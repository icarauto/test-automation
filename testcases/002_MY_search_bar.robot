*** Setting ***
Resource    ../module/homepage.robot
Resource    ../settings/settings.robot
Library	DebugLibrary

#Setup/Tear Down
Test Setup	Test Case Setup	${platform}	${env}
# Test Teardown	Close Browser

# robot --nostatusrc -d Reports --timestampoutputs --log mylog.html --report report.html -i tc01 -V vars/iot.yaml -v remote_url:None  .\TestCases\001_tc_homepage.robot
*** Variables ***
${home_header_sub_links_container}  //ul[@class="header__menu"]
${home_footer_app_container}    //footer//div[@class="footer__engage"]
@{window}
${window1}
${home_footer_links_container}    //div[contains(@class, 'grid') and contains(@class ,'push--bottom')]
${home_search_button}   //div[contains(@class, "search-button")]/button
# ${search_listings_result}   //*[@id="classified-listings-result"]
${search_listings_result}   //section[contains(@class, "listings__section")]/article
${classified_listings_home_link}   //*[@id="classified-listings"]/div/ol/li[1]/a/span[text()="Home"]
${search_listings_result_sections}    (//section[contains(@class, "listings__section")]/article)
${home_header_logo_40}   //a[contains(@class,'header__logo')]/img[@height="40"]
${home_header_logo_60}   //a[contains(@class,'header__logo')]/img[@height="60"]
${home_search_filters}    //*[@id="classified-listings"]//nav[contains(@class,"listings__fixed-left")]/div[contains(@class,"smenu")]

${home_listngs_header}   //nav[contains(@class,"header--listings") and contains(@class,"js-header--sticky-top")]
${home_nolistngs_header}   //nav[not(contains(@class,"header--listings")) and contains(@class,"js-header--sticky-top")]
${home_viewallcarsavailable_button}   //section[contains(@class, "home__used-cars")]//div[contains(@class, "flexbox__item")]/a[contains(text(),'View all cars available')]
${home_location_dropdown}   //form[@autocomplete="off"]//div[contains(@class, "location")]/div/div[contains(@class, "selectize-input")]/input

${home_search_price_option}   //form[@autocomplete="off"]//div[contains(@class, "classified-input")]//div[text()="Price"]
${home_search_min_price_textbox}   //form[@autocomplete="off"]//div[contains(@class, "classified-input") and @data-jranger="price"]//input[@placeholder="Min"]
${home_search_max_price_textbox}   //form[@autocomplete="off"]//div[contains(@class, "classified-input") and @data-jranger="price"]//input[@placeholder="Max"]
${home_search_min_price_value}   //form[@autocomplete="off"]//div[contains(@class, "classified-input") and @data-jranger="price"]//input[@placeholder="Min"]/following-sibling::div//div[contains(text(),"10,000")]

${home_search_Whatcarareyoulookingfor_textbox}   //div[contains(@class, "classified-input")]//input[@placeholder="What car are you looking for? " and @type="text"]
*** Keywords ***
launch carlist home page
	Go To	${url}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_carlist_logo}	timeout=12
	Set test variable	${title}
	${title}	Get Title

click search button
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_search_button}   timeout=12
    Run Keyword and continue on Failure     Set Focus To Element    ${home_search_button}
    Run Keyword and continue on Failure     Wait Until Keyword Succeeds   5x  500ms   Click Element   ${home_search_button}
    # Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_search_filters}  timeout=24
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${search_listings_result}  timeout=24

click View all cars available button
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_viewallcarsavailable_button}   timeout=12
	Run Keyword and continue on Failure	Mouse Over	${home_viewallcarsavailable_button}
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Click Element   ${home_viewallcarsavailable_button}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${search_listings_result_sections}	timeout=12

click home logo button
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_listngs_header}|${home_nolistngs_header}   timeout=12
    ${match1}    Get Element Count    ${home_listngs_header}
    ${match2}    Get Element Count    ${home_nolistngs_header}
    Run Keyword if   ${match1}==1   Run Keyword and continue on Failure    Wait Until Keyword Succeeds    5x  500ms   Click Element   ${home_header_logo_40}
    Run Keyword if   ${match2}==1   Run Keyword and continue on Failure    Wait Until Keyword Succeeds    5x  500ms   Click Element   ${home_header_logo_60}
    Run Keyword if   ${match1}==1    Run Keyword and continue on Failure     Wait Until Element Is Not Visible   ${home_listngs_header}   timeout=12
    Run Keyword if   ${match2}==1    Run Keyword and continue on Failure     Wait Until Element Is Not Visible   ${home_nolistngs_header}  timeout=12


select location from dropdown
    [Arguments]    ${state}
    Set test variable   ${statename}    //div[contains(@style,"visibility:")]//div[@class="selectize-dropdown-content"]/div[@data-value='${state}']
    Run Keyword and continue on Failure    Wait Until Element Is Visible   ${home_location_dropdown}
    Run Keyword and continue on Failure    Input text    ${home_location_dropdown}    ${state}
    # Run Keyword and continue on Failure    Wait Until Keyword Succeeds    5x   500ms    Click Element    ${home_location_dropdown}
    Run Keyword and continue on Failure    Wait Until Element Is Visible    ${statename}
    Run Keyword and continue on Failure    Wait Until Keyword Succeeds    5x   500ms    Click Element    ${statename}
    Set test variable   ${seectedstate}    //form[@autocomplete="off"]//div[@class="selectize-input items has-options full has-items"]/div[@data-value='${state}']

select car brand from dropdown
    [Arguments]    ${brand}
    Set test variable   ${brandname_loactor}    //div[contains(@style,"visibility:")]//div[@class="selectize-dropdown-content"]/div/div[contains(@data-value,'${brand}')]
    search option locator    Brand
    Run Keyword and continue on Failure    Wait Until Element Is Visible   ${search_option_locator}
    Run Keyword and continue on Failure    Input text    ${search_option_locator}    ${brand}
    Run Keyword and continue on Failure    Wait Until Element Is Visible    ${brandname_loactor}
    Run Keyword and continue on Failure    Wait Until Keyword Succeeds    5x   500ms    Click Element    ${brandname_loactor}
    Set test variable   ${seectedbrancd}    //form[@autocomplete="off"]//div[@class="selectize-input items has-options full has-items"]/div[@data-value='${brand}']

select car model from dropdown
    [Arguments]    ${model}
    Set test variable   ${brandname_loactor}    //div[contains(@style,"visibility:")]//div[@class="selectize-dropdown-content"]/div[contains(@data-value,'${model}')]
    search option locator    Model
    Run Keyword and continue on Failure    Wait Until Element Is Visible   ${search_option_locator}
    Run Keyword and continue on Failure    Wait Until Element Is Enabled   ${search_option_locator}    timeout=10
    Run Keyword and continue on Failure    Input text    ${search_option_locator}    ${model}
    Run Keyword and continue on Failure    Wait Until Element Is Visible    ${brandname_loactor}
    Run Keyword and continue on Failure    Wait Until Keyword Succeeds    5x   500ms    Click Element    ${brandname_loactor}
    Set test variable   ${seectedbrancd}    //form[@autocomplete="off"]//div[@class="selectize-input items has-options full has-items"]/div[@data-value='${model}']

search option locator
    [Documentation]   Brand, Model, All State, Price, Min, Max, What car are you looking for?
    [Arguments]    ${search_option}
    Set test variable   ${search_option_locator}   //form[@autocomplete="off"]//div[contains(@class, "classified-input")]//input[@placeholder="${search_option}"]


click price selectize box
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_search_price_option}   timeout=12
    Run Keyword and continue on Failure     Set Focus To Element    ${home_search_price_option}
    Run Keyword and continue on Failure     Wait Until Keyword Succeeds   5x  500ms   Click Element   ${home_search_price_option}
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_search_min_price_textbox}  timeout=12


input search keyword
    [Arguments]   ${search_text}
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_search_Whatcarareyoulookingfor_textbox}   timeout=12
    Run Keyword and continue on Failure     Set Focus To Element    ${home_search_Whatcarareyoulookingfor_textbox}
    Run Keyword and continue on Failure     input text   ${home_search_Whatcarareyoulookingfor_textbox}   ${search_text}
    Sleep   0.5

input car price
    [Arguments]   ${price_type}   ${price}
    Set test variable    ${home_search_price_type}   //form[@autocomplete="off"]//div[contains(@class, "classified-input") and @data-jranger="price"]//input[@placeholder="${price_type}"]
    Set test variable    ${entered_price_value}    //form[@autocomplete="off"]//div[contains(@class, "classified-input")]//div[contains(text(),"${price}")]
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${home_search_price_type}   timeout=12
    Run Keyword and continue on Failure     Set Focus To Element    ${home_search_price_type}
    Run Keyword and continue on Failure     input text   ${home_search_price_type}   ${price}
    Run Keyword and continue on Failure     Wait Until Element Is Visible   ${entered_price_value}  timeout=12

confirm filtered listings location
    [Arguments]    ${state}
    ${match1}    Get Element Count    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "location")]/div/div/div[contains(@class, "value")]
    ${match2}    Get text    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "location")]/div/div/div[contains(@class, "value")]
    ${match4}    Get Element Count    //*[contains(@id,"listing")]/div[contains(@class,"grid--full")]
    ${match5}    Get text        //*[contains(@id,"listing")]/div[contains(@class,"grid--full")]/header/div[contains(@class,"float--right")]/div[contains(@class,"milli")]/div[3]
    ${match6}    Get Element Count    //*[contains(@id,"listing")]/div[contains(@class,"grid--full")]/header/div[contains(@class,"float--right")]/div[contains(@class,"milli")]/div[text()][3]
    ${match7}    Get Element Count    //*[contains(@id,"listing")]/div[contains(@class,"grid--full")]/header/div[contains(@class,"float--right")]/div[contains(@class,"milli")]/div[text()[contains(.,'${state}')]]
    ${typecount}    Evaluate    type($match4)
    ${typecount}    Evaluate    type($match7)
    Run Keyword and continue on Failure    Should Be True     ${match4}==${match7}
    Run Keyword and continue on Failure    Should Be Equal As Strings    ${state}    ${match2}
    Run Keyword and continue on Failure    Should Be True     """${state}""".lower() == """${match2}""".lower()
    Run Keyword and continue on Failure    Should Be True     """${state}""".strip() == """${match2}""".strip()
    Run Keyword and continue on Failure    Should Be True     """${match4}""".strip() == """${match7}""".strip()

confirm filtered listings brand
    [Arguments]    ${state}
    ${match1}    Get Element Count    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "brand")]/div/div/div[contains(@class, "value")]
    ${match2}    Get text    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "brand")]/div/div/div[contains(@class, "value")]
    ${match4}    Get Element Count    //*[contains(@id,"listing")]/div[contains(@class,"grid--full")]
    ${typecount}    Evaluate    type($match2)
    ${typecount}    Evaluate    type($state)
    Run Keyword and continue on Failure    Should Be True     """${state}""".lower() == """${match2}""".lower()

confirm filtered listings model
    [Arguments]    ${model}
    ${match1}    Get Element Count    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "model")]/div/div/div[contains(@class, "value")]
    ${match2}    Get text    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "model")]/div/div/div[contains(@class, "value")]
    ${match4}    Get Element Count    //*[contains(@id,"listing")]/div[contains(@class,"grid--full")]
    ${typecount}    Evaluate    type($match2)
    ${typecount}    Evaluate    type($model)
    Run Keyword and continue on Failure    Should Be True     """${model}""".lower() == """${match2}""".lower()

confirm filtered listings
    [Arguments]    ${filter_option}    ${price}
    ${match1}    Get Element Count    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "${filter_option}")]/div/div/div[contains(@class, "value")]
    ${match2}    Get text    //div[@class="smenu__section"]//div[contains(@data-smenu-toggle, "${filter_option}")]/div/div/div[contains(@class, "value")]
    ${match4}    Get Element Count    //*[contains(@id,"listing")]/div[contains(@class,"grid--full")]
    ${typecount}    Evaluate    type($match2)
    ${typecount}    Evaluate    type($price)
    ${match2}=    Remove String      ${match2}    (Min)     (Max)
    # Run Keyword and continue on Failure    Should Be True     """${price}""" == """${match2}"""
    ${contains}=  Evaluate   "${price}" in """${match2}"""
    Run Keyword and continue on Failure    Should Be True      "${price}" in """${match2}"""
    Run Keyword and continue on Failure    Should Contain    ${match2}    ${price}
    ${contains}=    Run Keyword And Return Status    Should Contain    ${match2}    ${price}

*** Test Cases ***
TC01- Check on all listing without any filter
	[Documentation]	This test is to Check on all listing without any filter
	[tags]	carlist	medium	tc01
	launch carlist home page
	click search button
    click home logo button
    click View all cars available button

TC02- verify Search bar location value will become a search filter on listing page
    [Documentation]   Verify Search bar location value will become a search filter on listing page
    [tags]  carlist    medium    tc02
    launch carlist home page
    select location from dropdown    kuala lumpur
    click search button
    confirm filtered listings location    Kuala Lumpur


TC03- Get search listing based on listing condition in search bar
    [Documentation]    Get search listing based on listing condition in search bar
    [tags]  carlist    medium    tc03
    launch carlist home page

    @{ad_type_list}    Create List    All Condition   Used   New   Recon

    :FOR    ${ad_type}  IN  @{ad_type_list}
    \   Set Test Variable    ${check_box_clicked}    //div[contains(@class, "classified-search__body")]//div[contains(@class, "search-checkbox")]/label[text()[contains(.,'${ad_type}')]]
    \   Run Keyword and continue on Failure     Wait Until Element Is Visible   ${check_box_clicked}   timeout=12
    \   Run Keyword and continue on Failure     Mouse Over  ${check_box_clicked}
    \   Run Keyword and continue on Failure     Wait Until Keyword Succeeds   5x  500ms   Click Element   ${check_box_clicked}
    \   sleep   0.5
    \   click search button
    \   Set Test Variable    ${ad_type_clicked}  //div[@class="smenu__fields__container"]//div[@data-smenu-toggle]//div[text()="Condition"]//following-sibling::*//div[contains(@class,"smenu__value")]
    \   ${match}    Get text   ${ad_type_clicked}
    \   click home logo button

TC04- Get search listing by choose car make on search bar
    [Documentation]   Get search listing by choose car make on search bar
    [tags]  carlist    medium    tc04
    launch carlist home page
    # debug
    select car brand from dropdown    toyota
    click search button
    confirm filtered listings brand    toyota

TC05- Get search listing by choose car make and model on search bar
    [Documentation]   Get search listing by choose car make on search bar
    [tags]  carlist    medium    tc05
    launch carlist home page
    # debug
    select car brand from dropdown    toyota
    select car model from dropdown    avanza
    # debug
    click search button
    confirm filtered listings brand    toyota
    confirm filtered listings model    avanza


TC06- Get search listing by choose price from search bar
    [Documentation]   Get search listing by choose price from search bar
    [tags]  carlist    medium    tc06
    Set Test Variable    ${min}    121212
    Set Test Variable    ${max}    989800

    launch carlist home page
    click price selectize box
    input car price    Min   ${min}
    click search button
    confirm filtered listings    price    ${min}
    click home logo button

    click price selectize box
    input car price    Max   ${max}
    click search button
    confirm filtered listings    price    ${max}
    click home logo button

    click price selectize box
    input car price    Min   ${min}
    input car price    Max   ${max}
    click search button
    confirm filtered listings    price    ${max}
    click home logo button

TC07- Get search listing if search bar price filter min value > max value
    [Documentation]   Get search listing by choose price from search bar
    [tags]  carlist    medium    tc07
    Set Test Variable    ${min}    200000
    Set Test Variable    ${max}    5000

    launch carlist home page
    click price selectize box
    input car price    Min   ${min}
    input car price    Max   ${max}
    click search button
    confirm filtered listings    price    ${max}
    click home logo button

TC08- Get search listing by enter search keyword in search bar
    [Documentation]   Get search listing by enter search keyword in search bar
    [tags]  carlist    medium    tc08

    launch carlist home page
    input search keyword    Sales
    Run Keyword and continue on Failure    press keys    //i[@class="icon icon--magnifier"]   \\13
    click search button


TC09- Get search listing by select keyword suggestion in search bar
    [Documentation]   Get search listing by select keyword suggestion in search bar
    [tags]  carlist    medium    tc09

    launch carlist home page
    # debug
    input search keyword    Toy
    Run Keyword and continue on Failure   Wait Until Element Is Visible   //div[@class="selectize-dropdown-content"]/div[@data-value="Toyota RAV4"]
    Run Keyword and continue on Failure   click Element   //div[@class="selectize-dropdown-content"]/div[@data-value="Toyota RAV4"]
    # Run Keyword and continue on Failure    press keys    //i[@class="icon icon--magnifier"]   \\13
    click search button
    confirm filtered listings brand    toyota
    confirm filtered listings model    RAV4