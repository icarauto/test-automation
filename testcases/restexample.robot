*** Settings ***
Library         REST    https://www.carlist.my  ssl_verify=false
Documentation   REST HTTP JSON API JSON ROBOT Example
Library         HttpLibrary.HTTP
Library         RequestsLibrary
Library         json
Library         String
Library         BuiltIn
Library         Collections

*** Variables ***
${json}         { "id": 11, "name": "Gil Alexander" }
&{dict}         name=Julie Langford

${url}     http://lapi.icarasia.com/v3.6/my/en

*** Test Cases ***
GET authendication token
    [tags]  token
    Create Session      carlisttoken    ${url}
    # ${resp}=    Get Request   carlisttoken    /authentication/token
    # CreateSession    tmd    ${url}
    ${resp}=    RequestsLibrary.Post     carlisttoken    /authentication/token

    # RequestsLibrary.GET    carlisttoken    /authentication/token
    # Output      response


GET an existing user, notice how the schema gets more accurate
    [tags]  carlist   medium   tc01
    Create Session      carlist    https://www.carlist.my

    ${resp}=    Get Request   carlist    /cars-for-sale/malaysia_kuala-lumpur
    Output      response
    # Should Be Equal As Strings     ${resp.status_code}    200
    # Dictionary Should Contain Value    ${resp.json()}    kuala lumpur

GET an existing user, notice how the schema gets more accurate
    [tags]  carlist   medium   tc02
    GET         /cars-for-sale/malaysia_kuala-lumpur    timeout=2.0          # this creates a new instance
    Output      Rest Instances
    GET         /cars-for-sale/malaysia_kuala-lumpur     allow_redirects=false          # this creates a new instance
    Rest Instances
    GET         /cars-for-sale/malaysia_kuala-lumpur     allow_redirects=false          # this creates a new instance
    Output Schema
    GET         /cars-for-sale/malaysia_kuala-lumpur          # this creates a new instance
    OPTIONS      response
    GET         /cars-for-sale/malaysia_kuala-lumpur          # this creates a new instance
    Output      response body
    GET         /cars-for-sale/malaysia_johor          # this creates a new instance
    Run Keyword and continue on failure    Output      response body address
    GET         /cars-for-sale/malaysia_johor          # this creates a new instance
    Output      response
    GET         /cars-for-sale/johor          # this creates a new instance
    Output
    # Object      response body             # values are fully optional
    # String      response body addressRegion          kuala lumpur
    # String      response body name        kuala lumpur
    [Teardown]  Output                     # note the updated response schema

GET existing users, use JSONPath for very short but powerful queries
    GET         /users?_limit=5           # further assertions are to this
    Array       response body
    Integer     $[0].id                   1           # first id is 1
    String      $[0]..lat                 -37.3159    # any matching child
    Integer     $..id                     maximum=5   # multiple matches
    [Teardown]  Output   $[*].email        # outputs all emails as an array

POST with valid params to create a new user, can be output to a file
    POST        /users                    ${json}
    Integer     response status           201
    [Teardown]  Output   response body     ${OUTPUTDIR}/new_user.demo.json

PUT with valid params to update the existing user, values matter here
    PUT         /users/2                  { "isCoding": true }
    Boolean     response body isCoding    true
    PUT         /users/2                  { "sleep": null }
    Null        response body sleep
    PUT         /users/2                  { "pockets": "", "money": 0.02 }
    String      response body pockets     ${EMPTY}
    Number      response body money       0.02
    Missing     response body moving      # fails if property moving exists

PATCH with valid params, reusing response properties as a new payload
    &{res}=     GET   /users/3
    String      $.name                    Clementine Bauch
    PATCH       /users/4                  { "name": "${res.body['name']}" }
    String      $.name                    Clementine Bauch
    PATCH       /users/5                  ${dict}
    String      $.name                    ${dict.name}

DELETE the existing successfully, save the history of all requests
    DELETE      /users/6                  # status can be any of the below
    Integer     response status           200    202     204
    Rest instances  ${OUTPUTDIR}/all.demo.json  # all the instances so far