*** Setting ***
Resource	../settings/settings.robot

# robot --nostatusrc -d Reports --timestampoutputs --log mylog.html --report report.html -i tc01 -V vars/iot.yaml -v remote_url:None  .\TestCases\001_tc_homepage.robot
*** Variables ***
${home_carlist_logo}	//a[contains(@class,"header__logo-big")]//img[@alt="Carlist.my"]
${home_signin_link}	//nav[contains(@class,"header")]//a[text()="Sign In"]
${home_signin_button}   //button[text()="Sign In"]
${home_signin_modal_container}	//div[@class="modal__container"]//div[contains(@class,"modal__title") and text()="Sign In"]
${user_name_login}	//*[@id="user_name_login"]
${password_login}	//*[@id="password_login"]
${loggedin_user_avatar}	 //a[@class="hard"]/img
${title}
# ${title} = Find new & used cars for sale in Malaysia - Carlist.my
*** Keywords ***
launch carlist home page
	Go To	${url}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_carlist_logo}	timeout=12
	Set test variable	${title}
	${title}	Get Title

input username
	[Arguments]	${username}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${user_name_login}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Input text	${user_name_login}	${username}

input password
	[Arguments]	${password}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${password_login}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Input text	${password_login}	${password}

click sign in link
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_signin_link}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Click Element	${home_signin_link}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_signin_modal_container}	timeout=12

click sign in button
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${home_signin_button}	timeout=12
	Run Keyword and continue on Failure	Wait Until Keyword Succeeds	5x	500ms	Click Element   ${home_signin_button}
	Run Keyword and continue on Failure	Wait Until Element Is Visible	${loggedin_user_avatar}	timeout=12


skip user
Login with private user
Login with dealer
Login with broker
Login with sales agent user
Login with G+
Login with FB