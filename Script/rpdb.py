import pdb
import sys

IO = lambda s: (s.__stdin__, s.__stdout__)


def rpdb(F):
    
    # Robot Framework Python Debugger based 
    

    setattr(rpdb, 'set_trace', pdb.set_trace)
    builtinIO = IO(sys)

    def _inner(*args, **kwargs):
        robotIO = IO(sys)  # robot has hijacked stdin/stdout
        pdb.sys.stdin, pdb.sys.stdout = builtinIO
        retval = F(*args, **kwargs)
        sys.__stdin__, sys.__stdout__ = robotIO
        return retval

    return _inner
