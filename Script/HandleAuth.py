from SeleniumLibrary import SeleniumLibrary

class CustomSeleniumLibrary(SeleniumLibrary):
    def insert_into_promt(self, text):
        alert=None
        try:
            alert=self._current_browser().switch_to_alert()
            alert.send_keys(text)
        except  WebDriverException:
            raise RuntimeError('There were no alerts')

